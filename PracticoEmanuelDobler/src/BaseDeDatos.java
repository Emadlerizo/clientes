
import java.sql.Connection;
	import java.sql.Connection;
	import java.sql.DriverManager;
	import java.sql.PreparedStatement;
	import java.sql.SQLException;
	import com.mysql.jdbc.Statement;

	public class BaseDeDatos {

		// a java preparedstatement example
		public static void agregarALaBD(String Nombre, String apellido, String mail, int monto, int dni, int nroCuenta) 
				throws SQLException, ClassNotFoundException {
			try {
				// Creo la conexión a la base de datos dando como parámetro el String de
				// conexión y el Driver
				String myDriver = "org.gjt.mm.mysql.Driver";
				String myUrl = "jdbc:mysql://localhost/practicaemanueldobler";
				Class.forName(myDriver);
				// Doy los parámetros necesarios para la conexión con la BD, usuario y
				// contraseña
				Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
				String query = " insert into cliente (nombre, apellido, mail, monto, dni, nroCuenta)" + " values (?, ?, ?, ?, ?, ?)";

				// insert mediante preparedStatement donde doy como parámetro el query y los
				// valores del objeto cliente
				PreparedStatement preparedStmt = conn.prepareStatement(query);
				preparedStmt.setString(1, Nombre);
				preparedStmt.setString(2, apellido);
				preparedStmt.setString(3, mail);
				preparedStmt.setFloat(4, monto);
				preparedStmt.setInt(5, dni);
				preparedStmt.setInt(6, nroCuenta);
				// Ejecuto el preparedStatemen, de esta manera, inserto los valores a la base de
				// datos
				preparedStmt.executeUpdate();
				// cierro la conexión.
				conn.close();
			} catch (SQLException se) {
				// log the exception
				throw se;
			}
		}

		public static void updatecliente(int id, String Nombre, String apellido, String mail, int monto, int dni, int nroCuenta)
				throws SQLException, ClassNotFoundException {
			try {
				// Creo la conexión a la base de datos dando como parámetro el String de
				// conexión y el Driver
				String myDriver = "org.gjt.mm.mysql.Driver";
				String myUrl = "jdbc:mysql://localhost/practicaemanueldobler";
				Class.forName(myDriver);
				// Doy los parámetros necesarios para la conexión con la BD, usuario y
				// contraseña
				Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
				// Query de inserción de datos en la tabla cliente
				String query = "UPDATE cliente SET nombre = ?, apellido = ?, mail = ?, monto = ?, dni = ?, nroCuenta = ?  WHERE id = ?";
				// String query = "UPDATE cliente SET nombre = 'popo', apellido = 'asdf',
				// mail='fdsa' WHERE id = 31";

				// create our java preparedstatement using a sql update query
				PreparedStatement ps = conn.prepareStatement(query);

				// set the preparedstatement parameters

				ps.setString(1, Nombre);
				ps.setString(2, apellido);
				ps.setString(3, mail);
				ps.setFloat(4, monto);
				ps.setInt(5, dni);
				ps.setInt(6, nroCuenta);
				ps.setInt(7, id);

				// call executeUpdate to execute our sql update statement
				ps.executeUpdate();
				ps.close();
			} catch (SQLException | ClassNotFoundException e) {
				// log the exception
				System.err.println("Se ha generado la siguiente excepción:");
				System.err.println(e.getMessage());

			}
		}

		public static void borrarTablaDeLaBaseDeDatos() throws SQLException, ClassNotFoundException {
			try {
				// Creo la conexión a la base de datos dando como parámetro el String de
				// conexión y el Driver
				Statement stmt;
				String myDriver = "org.gjt.mm.mysql.Driver";
				String myUrl = "jdbc:mysql://localhost/practicaemanueldobler";
				Class.forName(myDriver);
				// Doy los parámetros necesarios para la conexión con la BD, usuario y
				// contraseña
				Connection conn = DriverManager.getConnection(myUrl, "root", "");

				String sql = "DELETE FROM cliente";
				PreparedStatement prest = conn.prepareStatement(sql);
				prest.executeUpdate();
				conn.close();

			} catch (SQLException s) {
				System.out.println("SQL statement is not executed!");
			}
		}
}